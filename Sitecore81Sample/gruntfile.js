module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-concat-in-order');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        requirejs: {
            compile: {
                options: {
                    baseUrl: "Scripts",
                    //mainConfigFile: "path/to/config.js",
                    //name: "path/to/almond", // assumes a production build using almond
                    out: "Scripts/optimized.js",
                    include: [
                        'media',
                    ],
                }
            }
        },
        concat_in_order: {
            your_target: {
                options: {
                },
                files: {
                    'Content/Site.concat.css': ['Content/generics.css', 'Content/Site.css']
                }
            }
        },
        cssmin : {
		    target : {
                src: ["Content/Site.concat.css"],
			    dest : "Content/Site.min.css"
		    }
	    }
    });

    grunt.registerTask('default', ["concat_in_order", "cssmin", 'requirejs']);

};