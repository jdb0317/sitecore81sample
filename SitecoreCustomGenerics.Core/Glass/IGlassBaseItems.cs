﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sitecore81.Core.Glass
{
    /// <summary>
    /// Base Page
    /// </summary>
    [SitecoreType(TemplateId = IBasePageConstants.TemplateIdString)]
    public partial interface IBasePage : IBasePageState
    {
        [SitecoreField(IBasePageConstants.TitleFieldName)]
        string Title { get; set; }

        [SitecoreField(IBasePageConstants.BodyTextFieldName)]
        string BodyText { get; set; }
    }
    public static partial class IBasePageConstants
    {
        public const string TemplateIdString = "0557651C-37B5-4AFF-BEF6-E1C004CD36A5";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Base Page";

        public const string TitleFieldName = "Title";
        public const string BodyTextFieldName = "BodyText";
    }

    /// <summary>
    /// Base Page State
    /// </summary>
    [SitecoreType(TemplateId = IBasePageStateConstants.TemplateIdString)]
    public partial interface IBasePageState : IGlassBase
    {
        [SitecoreField(IBasePageStateConstants.ActiveFieldName)]
        bool Active { get; set; }

        [SitecoreField(IBasePageStateConstants.Display_In_Menu_TextFieldName)]
        bool Display_In_Menu { get; set; }
    }
    public static partial class IBasePageStateConstants
    {
        public const string TemplateIdString = "FDC8FA06-529B-46B9-920A-643FD3C529BC";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Base Page State";

        public const string ActiveFieldName = "Active";
        public const string Display_In_Menu_TextFieldName = "Display In Menu";
    }

    [SitecoreType(TemplateId = IBase_Generic_Additional_Css_FieldsConstants.TemplateIdString)] //, Cachable = true
    public partial interface IBase_Generic_Additional_Css_Fields : IGlassBase
    {
        [SitecoreField(IBase_Generic_Additional_Css_FieldsConstants.AdditionalCssFieldName)]
        string AdditionalCss { get; set; }

        [SitecoreField(IBase_Generic_Additional_Css_FieldsConstants.CssClassesFieldName)]
        IEnumerable<ICssClass> CssClasses { get; set; }

        [SitecoreField(IBase_Generic_Additional_Css_FieldsConstants.InLineCssStylingFieldName)]
        string InLineCssStyling { get; set; }
    }


    public static partial class IBase_Generic_Additional_Css_FieldsConstants
    {
        public const string TemplateIdString = "ac18d859-9cf6-4af8-8490-b7c1e380f54b";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Base Generic Additional Css Fields";

        public static readonly ID AdditionalCssFieldId = new ID("5cc1831f-2b06-4f11-83fd-db1d76c4b741");
        public const string AdditionalCssFieldName = "AdditionalCss";

        public static readonly ID CssClassesFieldId = new ID("7f6e50f5-275a-4191-90fb-887e068e2005");
        public const string CssClassesFieldName = "CssClasses";

        public static readonly ID InLineCssStylingFieldId = new ID("c73dec73-c89c-480c-8efa-50bedb57e2b5");
        public const string InLineCssStylingFieldName = "InLineCssStyling";
    }
}
