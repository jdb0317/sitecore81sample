﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sitecore81.Core.Glass
{
    /// <summary>
    /// Generic Container
    /// </summary>
    [SitecoreType(TemplateId = IGeneric_Container_Rendering_ParamsConstants.TemplateIdString)]
    public partial interface IGeneric_Container_Rendering_Params : IGlassBase
    {
        [SitecoreField(IGeneric_Container_Rendering_ParamsConstants.CssClassesFieldName)]
        IEnumerable<Guid> CssClasses { get; set; }

        [SitecoreField(IGeneric_Container_Rendering_ParamsConstants.DesktopImageFieldName)]
        Image DesktopImage { get; set; }

        [SitecoreField(IGeneric_Container_Rendering_ParamsConstants.FullWidthContainerFieldName)]
        bool FullWidthContainer { get; set; }

        [SitecoreField(IGeneric_Container_Rendering_ParamsConstants.ForceHeightToMatchBgImageFieldName)]
        bool ForceHeightToMatchBgImage { get; set; }

        [SitecoreField(IGeneric_Container_Rendering_ParamsConstants.InLineCssStylingFieldName)]
        string InLineCssStyling { get; set; }

        [SitecoreField(IGeneric_Container_Rendering_ParamsConstants.MobileImageFieldName)]
        Image MobileImage { get; set; }

        [SitecoreField(IGeneric_Container_Rendering_ParamsConstants.New_Placeholder_Container_NameFieldName)]
        string New_Placeholder_Container_Name { get; set; }

        [SitecoreField(IGeneric_Container_Rendering_ParamsConstants.TabletImageFieldName)]
        Image TabletImage { get; set; }
    }
    public static partial class IGeneric_Container_Rendering_ParamsConstants
    {
        public const string TemplateIdString = "9d6224ee-f960-438f-bab2-61094219cc38";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Generic Container Rendering Params";

        public const string CssClassesFieldName = "CssClasses";
        public const string ForceHeightToMatchBgImageFieldName = "ForceHeightToMatchBgImage";
        public const string DesktopImageFieldName = "DesktopImage";
        public const string FullWidthContainerFieldName = "FullWidthContainer";
        public const string InLineCssStylingFieldName = "InLineCssStyling";
        public const string MobileImageFieldName = "MobileImage";
        public const string New_Placeholder_Container_NameFieldName = "New Placeholder Container Name";
        public const string TabletImageFieldName = "TabletImage";
        public const string PlaceholderFieldName = "Placeholder";
    }

    /// <summary>
    /// Generic Tile Row
    /// </summary>
    [SitecoreType(TemplateId = IGeneric_Tile_Row_Rendering_ParamsConstants.TemplateIdString)]
    public partial interface IGeneric_Tile_Row_Rendering_Params : IGlassBase
    {
        [SitecoreField(IGeneric_Tile_Row_Rendering_ParamsConstants.Breakpoint_OverridesFieldName)]
        IEnumerable<Guid> Breakpoint_Overrides { get; set; }

        [SitecoreField(IGeneric_Tile_Row_Rendering_ParamsConstants.ForceHeightToMatchBgImageFieldName)]
        bool ForceHeightToMatchBgImage { get; set; }

        [SitecoreField(IGeneric_Tile_Row_Rendering_ParamsConstants.CssClassesFieldName)]
        IEnumerable<Guid> CssClasses { get; set; }

        [SitecoreField(IGeneric_Tile_Row_Rendering_ParamsConstants.DesktopImageFieldName)]
        Image DesktopImage { get; set; }

        [SitecoreField(IGeneric_Tile_Row_Rendering_ParamsConstants.FullWidthContainerFieldName)]
        bool FullWidthContainer { get; set; }

        [SitecoreField(IGeneric_Tile_Row_Rendering_ParamsConstants.PaddingBetweenCellsFieldName)]
        bool PaddingBetweenCells { get; set; }

        [SitecoreField(IGeneric_Tile_Row_Rendering_ParamsConstants.InLineCssStylingFieldName)]
        string InLineCssStyling { get; set; }

        [SitecoreField(IGeneric_Tile_Row_Rendering_ParamsConstants.MobileImageFieldName)]
        Image MobileImage { get; set; }

        [SitecoreField(IGeneric_Tile_Row_Rendering_ParamsConstants.OverrideFlexBreakpointsFieldName)]
        bool OverrideFlexBreakpoints { get; set; }

        [SitecoreField(IGeneric_Tile_Row_Rendering_ParamsConstants.RowTypeFieldName)]
        Guid RowType { get; set; }

        [SitecoreField(IGeneric_Tile_Row_Rendering_ParamsConstants.TabletImageFieldName)]
        Image TabletImage { get; set; }

        [SitecoreField(IGeneric_Tile_Row_Rendering_ParamsConstants.TilesFieldName)]
        IEnumerable<Guid> Tiles { get; set; }

        [SitecoreField(IGeneric_Tile_Row_Rendering_ParamsConstants.LinkDestinationFieldName)]
        Link LinkDestination { get; set; }
    }


    public static partial class IGeneric_Tile_Row_Rendering_ParamsConstants
    {

        public const string TemplateIdString = "689953ba-c90c-4091-8db9-dbe0febdc0aa";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Generic Tile Row Rendering Params";

        public static readonly ID Breakpoint_OverridesFieldId = new ID("95d5ed54-97f1-4966-9e7b-cde8e0872a91");
        public const string Breakpoint_OverridesFieldName = "Breakpoint Overrides";
        public const string ForceHeightToMatchBgImageFieldName = "ForceHeightToMatchBgImage";
        public const string CssClassesFieldName = "CssClasses";
        public const string DesktopImageFieldName = "DesktopImage";
        public const string FullWidthContainerFieldName = "FullWidthContainer";
        public const string PaddingBetweenCellsFieldName = "PaddingBetweenCells";
        public const string InLineCssStylingFieldName = "InLineCssStyling";
        public const string MobileImageFieldName = "MobileImage";
        public const string OverrideFlexBreakpointsFieldName = "OverrideFlexBreakpoints";
        public const string RowTypeFieldName = "RowType";
        public const string TabletImageFieldName = "TabletImage";
        public const string TilesFieldName = "Tiles";
        public const string LinkDestinationFieldName = "LinkDestination";
    }

    /// <summary>
    /// CTA Image Slider
    /// </summary>
    [SitecoreType(TemplateId = ICTA_Image_SliderConstants.TemplateIdString)]
    public partial interface ICTA_Image_Slider : IGlassBase
    {
        [SitecoreField(ICTA_Image_SliderConstants.TitleFieldName)]
        string Title { get; set; }

        [SitecoreField(ICTA_Image_SliderConstants.BlurbFieldName)]
        string Blurb { get; set; }

        [SitecoreField(ICTA_Image_SliderConstants.CTA_TextFieldName)]
        string CTA_Text { get; set; }

        [SitecoreField(ICTA_Image_SliderConstants.CTA_DestinationFieldName)]
        Link CTA_Destination { get; set; }

        [SitecoreField(ICTA_Image_SliderConstants.ImageFieldName)]
        Image Image { get; set; }
    }
    public static partial class ICTA_Image_SliderConstants
    {
        public const string TemplateIdString = "B56FEEB0-0C52-49AF-8F15-01E1B3849D5A";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "CTA Image Slider";

        public const string TitleFieldName = "Title";
        public const string BlurbFieldName = "Blurb";
        public const string CTA_TextFieldName = "CTA Text";
        public const string CTA_DestinationFieldName = "CTA Destination";
        public const string ImageFieldName = "Image";
    }

    /// <summary>
    /// CTA Image Slider Group
    /// </summary>
    [SitecoreType(TemplateId = ICTA_Image_Slider_GroupConstants.TemplateIdString)]
    public partial interface ICTA_Image_Slider_Group : IGlassBase
    {
        [SitecoreField(ICTA_Image_Slider_GroupConstants.SlidesFieldName)]
        IEnumerable<ICTA_Image_Slider> Slides { get; set; }
    }
    public static partial class ICTA_Image_Slider_GroupConstants
    {
        public const string TemplateIdString = "A49EEC61-F5A5-4C59-B7FB-D3FEF142F4D3";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "CTA Image Slider Group";

        public const string SlidesFieldName = "Slides";
    }

    /// <summary>
    /// Css Class item
    /// </summary>
    [SitecoreType(TemplateId = ICssClassConstants.TemplateIdString)]
    public partial interface ICssClass : IGlassBase
    {
        [SitecoreField(ICssClassConstants.ClassFieldName)]
        string Class { get; set; }
    }
    public static partial class ICssClassConstants
    {

        public const string TemplateIdString = "75b1ec7b-be0b-41d0-93f9-1fea340f96f9";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "CssClass";

        public const string ClassFieldName = "Class";
    }

    /// <summary>
    /// Media Type selection for generic tile
    /// </summary>
    [SitecoreType(TemplateId = IMedia_TypeConstants.TemplateIdString)] //, Cachable = true
    public partial interface IMedia_Type : IGlassBase
    {
        [SitecoreField(IMedia_TypeConstants.MediaNameFieldName)]
        string MediaName { get; set; }
    }
    public static partial class IMedia_TypeConstants
    {
        public const string TemplateIdString = "df26e02c-feda-4911-94c5-69a7cf89bdbf";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Media Type";

        public static readonly ID MediaNameFieldId = new ID("462c5967-d386-48fa-af46-b45d70a073db");
        public const string MediaNameFieldName = "MediaName";
    }

    /// <summary>
    /// Generic Row Definition
    /// </summary>
    [SitecoreType(TemplateId = IGeneric_Row_DefinitionConstants.TemplateIdString)]
    public partial interface IGeneric_Row_Definition : IGlassBase
    {
        [SitecoreField(IGeneric_Row_DefinitionConstants.AdditionalCssFieldName)]
        string AdditionalCss { get; set; }

        [SitecoreField(IGeneric_Row_DefinitionConstants.ColumnsFieldName)]
        IEnumerable<IGeneric_Column_Definition> Columns { get; set; }

        [SitecoreField(IGeneric_Row_DefinitionConstants.FlexBreakpointsFieldName)]
        IEnumerable<IGeneric_Row_Flex_Breakpoint> FlexBreakpoints { get; set; }

        [SitecoreField(IGeneric_Row_DefinitionConstants.LayoutNameFieldName)]
        string LayoutName { get; set; }

        [SitecoreField(IGeneric_Row_DefinitionConstants.CssClassesFieldName)]
        IEnumerable<ICssClass> CssClasses { get; set; }
    }
    public static partial class IGeneric_Row_DefinitionConstants
    {
        public const string TemplateIdString = "0fc50f9f-c6a9-4896-8eec-ec16d7956033";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Generic Row Defnition";

        public static readonly ID AdditionalCssFieldId = new ID("72fb5ab9-f0c6-4a55-8d70-8ce315d24c65");
        public const string AdditionalCssFieldName = "AdditionalCss";

        public static readonly ID ColumnsFieldId = new ID("875438c6-bf28-4f66-934e-88d9ac79e28f");
        public const string ColumnsFieldName = "Columns";

        public static readonly ID CssClassesFieldId = new ID("bc8770be-ffff-4e0d-b34e-e1f31dab0931");
        public const string CssClassesFieldName = "CssClasses";

        public static readonly ID FlexBreakpointsFieldId = new ID("2f07c0f7-5e02-44a9-b745-509d126a7f07");
        public const string FlexBreakpointsFieldName = "FlexBreakpoints";

        public static readonly ID LayoutNameFieldId = new ID("f1f2e0c5-3971-4f9f-8862-c2e27625cb10");
        public const string LayoutNameFieldName = "LayoutName";
    }

    /// <summary>
    /// Generic Column Definition
    /// </summary>
    [SitecoreType(TemplateId = IGeneric_Column_DefinitionConstants.TemplateIdString)]
    public partial interface IGeneric_Column_Definition : IGlassBase
    {
        [SitecoreField(IGeneric_Column_DefinitionConstants.ColumnDefinitionFieldName)]
        string ColumnDefinition { get; set; }
    }
    public static partial class IGeneric_Column_DefinitionConstants
    {
        public const string TemplateIdString = "28ef579e-e0c6-47c3-ad6d-ca7ab3b5bf9c";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Generic Column Definition";

        public static readonly ID ColumnDefinitionFieldId = new ID("1b44c6d4-ce13-4044-a42d-f6e46460e353");
        public const string ColumnDefinitionFieldName = "ColumnDefinition";
    }

    /// <summary>
    /// Generic Row Flex Breakpoint
    /// </summary>
    [SitecoreType(TemplateId = IGeneric_Row_Flex_BreakpointConstants.TemplateIdString)]
    public partial interface IGeneric_Row_Flex_Breakpoint : IGlassBase
    {
        [SitecoreField(IGeneric_Row_Flex_BreakpointConstants.BreakpointCssClassNameFieldName)]
        string BreakpointCssClassName { get; set; }

    }
    public static partial class IGeneric_Row_Flex_BreakpointConstants
    {
        public const string TemplateIdString = "4968cd57-5811-44c8-a19c-3b5ea214ae2a";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Generic Row Flex Breakpoint";

        public static readonly ID BreakpointCssClassNameFieldId = new ID("fd798ae2-97c8-40be-a629-daf51a7bbb84");
        public const string BreakpointCssClassNameFieldName = "BreakpointCssClassName";
    }

    /// <summary>
    /// Generic Rich Text Tile
    /// </summary>
    [SitecoreType(TemplateId = IGeneric_Rich_Text_TileConstants.TemplateIdString)] //, Cachable = true
    public partial interface IGeneric_Rich_Text_Tile : IGlassBase
    {
        [SitecoreField(IGeneric_Rich_Text_TileConstants.CopyFieldName)]
        string Copy { get; set; }

        [SitecoreField(IGeneric_Rich_Text_TileConstants.CssClassesFieldName)]
        IEnumerable<ICssClass> CssClasses { get; set; }

        [SitecoreField(IGeneric_Rich_Text_TileConstants.AdditionalCssFieldName)]
        string AdditionalCss { get; set; }

        [SitecoreField(IGeneric_Rich_Text_TileConstants.InLineCssStylingFieldName)]
        string InLineCssStyling { get; set; }

        [SitecoreField(IGeneric_Rich_Text_TileConstants.BackgroundImageFieldName)]
        Image BackgroundImage { get; set; }
    }
    public static partial class IGeneric_Rich_Text_TileConstants
    {
        public const string TemplateIdString = "3a519353-829b-4209-9ce2-3b6978773868";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Generic Rich Text Tile";

        public static readonly ID CopyFieldId = new ID("d6bd67d0-8d2f-4aa0-a32b-ab1209a63c55");
        public const string CopyFieldName = "Copy";

        public static readonly ID CssClassesFieldId = new ID("ac0ff878-d76a-45ef-9fd2-8b9efca90799");
        public const string CssClassesFieldName = "CssClasses";

        public const string AdditionalCssFieldName = "AdditionalCss";

        public static readonly ID InLineCssStylingFieldId = new ID("11e97a7f-5ae6-43c8-ba40-0557ba9226d1");
        public const string InLineCssStylingFieldName = "InLineCssStyling";

        public static readonly ID BackgroundImageFieldId = new ID("A4F49AE0-A586-4A5A-AA5C-4582000F3206");
        public const string BackgroundImageFieldName = "BackgroundImage";
    }

    /// <summary>
    /// Generic Media Tile
    /// </summary>
    [SitecoreType(TemplateId = IGeneric_Media_TileConstants.TemplateIdString)] //, Cachable = true
    public partial interface IGeneric_Media_Tile : IGlassBase
    {
        [SitecoreField(IGeneric_Media_TileConstants.CssClassesFieldName)]
        IEnumerable<ICssClass> CssClasses { get; set; }

        [SitecoreField(IGeneric_Media_TileConstants.DesktopImageFieldName)]
        Image DesktopImage { get; set; }

        [SitecoreField(IGeneric_Media_TileConstants.InLineCssStylingFieldName)]
        string InLineCssStyling { get; set; }

        [SitecoreField(IGeneric_Media_TileConstants.MediaTypeFieldName)]
        IMedia_Type MediaType { get; set; }

        [SitecoreField(IGeneric_Media_TileConstants.MobileImageFieldName)]
        Image MobileImage { get; set; }

        [SitecoreField(IGeneric_Media_TileConstants.TabletImageFieldName)]
        Image TabletImage { get; set; }

        [SitecoreField(IGeneric_Media_TileConstants.VideoEmbedUrlFieldName)]
        string VideoEmbedUrl { get; set; }
    }
    public static partial class IGeneric_Media_TileConstants
    {
        public const string TemplateIdString = "38a9aca7-25f8-42cf-b1ca-8c9726928cbf";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Generic Media Tile";

        public static readonly ID CssClassesFieldId = new ID("4fc441f1-9122-4474-a136-7129f4f6cac4");
        public const string CssClassesFieldName = "CssClasses";

        public static readonly ID DesktopImageFieldId = new ID("45eb3e19-561d-4f64-ad32-4bf5bbce0b72");
        public const string DesktopImageFieldName = "DesktopImage";

        public static readonly ID InLineCssStylingFieldId = new ID("78c5ee3f-1f7b-48b2-bb17-85f3d71e55a2");
        public const string InLineCssStylingFieldName = "InLineCssStyling";

        public static readonly ID MediaTypeFieldId = new ID("ed105176-b1c0-4715-9b83-ae17f700b64b");
        public const string MediaTypeFieldName = "MediaType";

        public static readonly ID MobileImageFieldId = new ID("abbdd519-12ca-4731-9cf2-415678554182");
        public const string MobileImageFieldName = "MobileImage";

        public static readonly ID TabletImageFieldId = new ID("8a6b2c75-f3ca-464f-b891-c69d65d9ffbc");
        public const string TabletImageFieldName = "TabletImage";

        public static readonly ID VideoEmbedUrlFieldId = new ID("45d20699-e12c-42af-aeca-080db4e5549f");
        public const string VideoEmbedUrlFieldName = "VideoEmbedUrl";
    }

    [SitecoreType(TemplateId = IGeneric_Button_TileConstants.TemplateIdString)] //, Cachable = true
    public partial interface IGeneric_Button_Tile : IGlassBase, IBase_Generic_Additional_Css_Fields
    {
        /// <summary>
        /// The Button Text field.
        /// <para></para>
        /// <para>Field Type: Single-Line Text</para>		
        /// <para>Field ID: a67c0ed1-58f3-49e3-8c66-977c9c8ce15a</para>
        /// <para>Custom Data: </para>
        /// </summary>
        [SitecoreField(IGeneric_Button_TileConstants.Button_TextFieldName)]
        string Button_Text { get; set; }

        /// <summary>
        /// The Button Type field.
        /// <para></para>
        /// <para>Field Type: Droplink</para>		
        /// <para>Field ID: 6bd03fee-9612-4b8c-a46c-a5706b71587e</para>
        /// <para>Custom Data: </para>
        /// </summary>
        [SitecoreField(IGeneric_Button_TileConstants.Button_TypeFieldName)]
        IButton_Type Button_Type { get; set; }

        /// <summary>
        /// The Destination field.
        /// <para></para>
        /// <para>Field Type: General Link</para>		
        /// <para>Field ID: 92a798a8-0ee1-40b1-96b7-1be370ebf118</para>
        /// <para>Custom Data: </para>
        /// </summary>
        [SitecoreField(IGeneric_Button_TileConstants.DestinationFieldName)]
        Link Destination { get; set; }
    }

    public interface IGenericButtonTile : IGeneric_Button_Tile
    {
    }


    public static partial class IGeneric_Button_TileConstants
    {
        public const string TemplateIdString = "e7202edd-a465-4cb1-9238-e26d103ac0c9";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Generic Button Tile";

        public static readonly ID Button_TextFieldId = new ID("a67c0ed1-58f3-49e3-8c66-977c9c8ce15a");
        public const string Button_TextFieldName = "Button Text";

        public static readonly ID Button_TypeFieldId = new ID("6bd03fee-9612-4b8c-a46c-a5706b71587e");
        public const string Button_TypeFieldName = "Button Type";

        public static readonly ID DestinationFieldId = new ID("92a798a8-0ee1-40b1-96b7-1be370ebf118");
        public const string DestinationFieldName = "Destination";

        public static readonly ID AdditionalCssFieldId = new ID("5cc1831f-2b06-4f11-83fd-db1d76c4b741");
        public const string AdditionalCssFieldName = "AdditionalCss";

        public static readonly ID CssClassesFieldId = new ID("7f6e50f5-275a-4191-90fb-887e068e2005");
        public const string CssClassesFieldName = "CssClasses";

        public static readonly ID InLineCssStylingFieldId = new ID("c73dec73-c89c-480c-8efa-50bedb57e2b5");
        public const string InLineCssStylingFieldName = "InLineCssStyling";
    }

    [SitecoreType(TemplateId = IButton_TypeConstants.TemplateIdString)] //, Cachable = true
    public partial interface IButton_Type : IGlassBase
    {
        [SitecoreField(IButton_TypeConstants.ButtonTypeFieldName)]
        string ButtonType { get; set; }
    }


    public static partial class IButton_TypeConstants
    {
        public const string TemplateIdString = "d22f598d-1024-4aea-80bb-d970b35bd003";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Button Type";

        public static readonly ID ButtonTypeFieldId = new ID("91d28353-a3b5-4397-9899-4866e61708d5");
        public const string ButtonTypeFieldName = "ButtonType";
    }

    public static partial class ILanding_Page_Intro_With_VideoConstants
    {
        public const string TemplateIdString = "A8533D2F-C0CF-4EBA-92CC-9C7EAA3E6A4F";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Landing Page Intro With Video";

        public static readonly ID Headline_Line_1FieldId = new ID("9F7927AA-164A-4ACF-B0D2-EAF34034D6B6");
        public const string Headline_Line_1FieldName = "Headline Line 1";

        public static readonly ID Headline_Line_2FieldId = new ID("9B60F720-B892-45C5-95C0-4C6DF54D2C6B");
        public const string Headline_Line_2FieldName = "Headline Line 2";

        public static readonly ID Video_UrlFieldId = new ID("6153E846-0CC3-4DA3-B28E-46A810F4E269");
        public const string Video_UrlFieldName = "Video Url";

        public static readonly ID Video_CTA_TextFieldId = new ID("D75A99C9-ED85-46A5-8222-9DC91F22D1FA");
        public const string Video_CTA_TextFieldName = "Video CTA Text";

        public static readonly ID Description_HeaderFieldId = new ID("DEA4D05E-6513-44BF-BD57-D7FC8237F7F6");
        public const string Description_HeaderFieldName = "Description Header";

        public static readonly ID Description_TextFieldId = new ID("BA5A2EAB-D59F-4A33-8DC3-B858AF720BF4");
        public const string Description_TextFieldName = "Description Text";

        public static readonly ID Description_Lower_TextFieldId = new ID("17A8BAE8-A1CD-49D1-A771-9A9526062A69");
        public const string Description_Lower_TextFieldName = "Description Lower Text";

        public static readonly ID Bottom_CTA_TextFieldId = new ID("747C10B1-FD29-4F49-8A7C-BFC9059DB33D");
        public const string Bottom_CTA_TextFieldName = "Bottom CTA Text";

        public static readonly ID Bottom_CTA_DestinationFieldId = new ID("EBC5ED01-8C02-4401-A11F-6C447E793AAD");
        public const string Bottom_CTA_DestinationFieldName = "Bottom CTA Destination";

        public static readonly ID Background_ImageFieldId = new ID("A6F0228A-3249-483D-B60F-BD94096A5DF3");
        public const string Background_ImageFieldName = "Background Image";

        public static readonly ID Additional_Css_ClassFieldId = new ID("A6F0228A-3249-483D-B60F-BD94096A5DF3");
        public const string Additional_Css_ClassFieldName = "Background Image";
    }

    [SitecoreType(TemplateId = ILanding_Page_Intro_With_VideoConstants.TemplateIdString)]
    public partial interface ILanding_Page_Intro_With_Video : IGlassBase
    {
        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Headline_Line_1FieldName)]
        string Headline_Line_1 { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Headline_Line_2FieldName)]
        string Headline_Line_2 { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Video_UrlFieldName)]
        string Video_Url { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Video_CTA_TextFieldName)]
        string Video_CTA_Text { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Description_HeaderFieldName)]
        string Description_Header { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Description_TextFieldName)]
        string Description_Text { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Description_Lower_TextFieldName)]
        string Description_Lower_Text { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Bottom_CTA_TextFieldName)]
        string Bottom_CTA_Text { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Bottom_CTA_DestinationFieldName)]
        Link Bottom_CTA_Destination { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Background_ImageFieldName)]
        Image Background_Image { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Additional_Css_ClassFieldName)]
        string Additional_Css_Classes { get; set; }
    }

    public static partial class ILanding_Page_Media_Section_Rendering_ParametersConstants
    {
        public const string TemplateIdString = "476FBB24-7A19-4213-92B8-D422544BC19A";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Landing Page Media Section Rendering Parameters";

        public const string UpperHeadlineFieldName = "UpperHeadline";
        public const string LowerHeadlineFieldName = "LowerHeadline"; 
        public const string CopyFieldName = "Copy";
        public const string Video1BackgroundImageFieldName = "Video1BackgroundImage";
        public const string Video1ForegroundTextFieldName = "Video1ForegroundText";
        public const string Video1SubTextFieldName = "Video1SubText";
        public const string Video1EmbedCodeFieldName = "Video1EmbedCode";
        public const string Video2BackgroundImageFieldName = "Video2BackgroundImage";
        public const string Video2ForegroundTextFieldName = "Video2ForegroundText";
        public const string Video2SubTextFieldName = "Video2SubText";
        public const string Video2EmbedCodeFieldName = "Video2EmbedCode";
        public const string Video3BackgroundImageFieldName = "Video3BackgroundImage";
        public const string Video3ForegroundTextFieldName = "Video3ForegroundText";
        public const string Video3SubTextFieldName = "Video3SubText";
        public const string Video3EmbedCodeFieldName = "Video3EmbedCode";
        public const string Video4BackgroundImageFieldName = "Video4BackgroundImage";
        public const string Video4ForegroundTextFieldName = "Video4ForegroundText";
        public const string Video4SubTextFieldName = "Video4SubText";
        public const string Video4EmbedCodeFieldName = "Video4EmbedCode";
    }

    [SitecoreType(TemplateId = ILanding_Page_Media_Section_Rendering_ParametersConstants.TemplateIdString)]
    public partial interface ILanding_Page_Media_Section_Rendering_Parameters : IGlassBase
    {
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.UpperHeadlineFieldName)]
        string UpperHeadline { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.LowerHeadlineFieldName)]
        string LowerHeadline { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.CopyFieldName)]
        string Copy { get; set; }

        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video1BackgroundImageFieldName)]
        Image Video1BackgroundImage { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video1ForegroundTextFieldName)]
        string Video1ForegroundTextFieldName { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video1SubTextFieldName)]
        string Video1SubTextFieldName { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video1EmbedCodeFieldName)]
        string Video1EmbedCodeFieldName { get; set; }

        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video2BackgroundImageFieldName)]
        Image Video2BackgroundImage { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video2ForegroundTextFieldName)]
        string Video2ForegroundTextFieldName { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video2SubTextFieldName)]
        string Video2SubTextFieldName { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video2EmbedCodeFieldName)]
        string Video2EmbedCodeFieldName { get; set; }

        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video3BackgroundImageFieldName)]
        Image Video3BackgroundImage { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video3ForegroundTextFieldName)]
        string Video3ForegroundTextFieldName { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video3SubTextFieldName)]
        string Video3SubTextFieldName { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video3EmbedCodeFieldName)]
        string Video3EmbedCodeFieldName { get; set; }

        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video4BackgroundImageFieldName)]
        Image Video4BackgroundImage { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video4ForegroundTextFieldName)]
        string Video4ForegroundTextFieldName { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video4SubTextFieldName)]
        string Video4SubTextFieldName { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video4EmbedCodeFieldName)]
        string Video4EmbedCodeFieldName { get; set; }
    }
}
