﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sitecore81.Core.Glass
{
    /// <summary>
    /// Base Page
    /// </summary>
    [SitecoreType(TemplateId = IBasePageConstants.TemplateIdString)]
    public partial interface IBasePage : IBasePageState
    {
        [SitecoreField(IBasePageConstants.TitleFieldName)]
        string Title { get; set; }

        [SitecoreField(IBasePageConstants.BodyTextFieldName)]
        string BodyText { get; set; }
    }
    public static partial class IBasePageConstants
    {
        public const string TemplateIdString = "0557651C-37B5-4AFF-BEF6-E1C004CD36A5";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Base Page";

        public const string TitleFieldName = "Title";
        public const string BodyTextFieldName = "BodyText";
    }

    /// <summary>
    /// Base Page State
    /// </summary>
    [SitecoreType(TemplateId = IBasePageStateConstants.TemplateIdString)]
    public partial interface IBasePageState : IGlassBase
    {
        [SitecoreField(IBasePageStateConstants.ActiveFieldName)]
        bool Active { get; set; }

        [SitecoreField(IBasePageStateConstants.Display_In_Menu_TextFieldName)]
        bool Display_In_Menu { get; set; }
    }
    public static partial class IBasePageStateConstants
    {
        public const string TemplateIdString = "FDC8FA06-529B-46B9-920A-643FD3C529BC";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Base Page State";

        public const string ActiveFieldName = "Active";
        public const string Display_In_Menu_TextFieldName = "Display In Menu";
    }

    [SitecoreType(TemplateId = IBase_Generic_Additional_Css_FieldsConstants.TemplateIdString)] //, Cachable = true
    public partial interface IBase_Generic_Additional_Css_Fields : IGlassBase
    {
        [SitecoreField(IBase_Generic_Additional_Css_FieldsConstants.AdditionalCssFieldName)]
        string AdditionalCss { get; set; }

        [SitecoreField(IBase_Generic_Additional_Css_FieldsConstants.CssClassesFieldName)]
        IEnumerable<ICssClass> CssClasses { get; set; }

        [SitecoreField(IBase_Generic_Additional_Css_FieldsConstants.InLineCssStylingFieldName)]
        string InLineCssStyling { get; set; }
    }


    public static partial class IBase_Generic_Additional_Css_FieldsConstants
    {
        public const string TemplateIdString = "ac18d859-9cf6-4af8-8490-b7c1e380f54b";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Base Generic Additional Css Fields";

        public static readonly ID AdditionalCssFieldId = new ID("5cc1831f-2b06-4f11-83fd-db1d76c4b741");
        public const string AdditionalCssFieldName = "AdditionalCss";

        public static readonly ID CssClassesFieldId = new ID("7f6e50f5-275a-4191-90fb-887e068e2005");
        public const string CssClassesFieldName = "CssClasses";

        public static readonly ID InLineCssStylingFieldId = new ID("c73dec73-c89c-480c-8efa-50bedb57e2b5");
        public const string InLineCssStylingFieldName = "InLineCssStyling";
    }


	[SitecoreType(TemplateId = IRendering_Parameters___No_ParamsConstants.TemplateIdString)] //, Cachable = true
    public partial interface IRendering_Parameters___No_Params : IGlassBase
    {
        [SitecoreField(IRendering_Parameters___No_ParamsConstants.CachingFieldName)]
        object /* UNKNOWN */ Caching { get; set; }

        [SitecoreField(IRendering_Parameters___No_ParamsConstants.PlaceholderFieldName)]
        string Placeholder { get; set; }

        [SitecoreField(IRendering_Parameters___No_ParamsConstants.Additional_ParametersFieldName)]
        System.Collections.Specialized.NameValueCollection Additional_Parameters { get; set; }

        [SitecoreField(IRendering_Parameters___No_ParamsConstants.PersonalizationFieldName)]
        object /* UNKNOWN */ Personalization { get; set; }

        [SitecoreField(IRendering_Parameters___No_ParamsConstants.TestsFieldName)]
        object /* UNKNOWN */ Tests { get; set; }

    }


    public static partial class IRendering_Parameters___No_ParamsConstants
    {
        public const string TemplateIdString = "1cda0751-daeb-42c5-b318-2e2f4bb526e8";
        public static readonly ID TemplateId = new ID(TemplateIdString);
        public const string TemplateName = "Rendering Parameters - No Params";


        public static readonly ID CachingFieldId = new ID("5a0e7c98-d77d-472e-8c2c-1f6ef6878ec7");
        public const string CachingFieldName = "Caching";


        public static readonly ID PlaceholderFieldId = new ID("8028dad5-22be-4bc4-96b3-bddfdbf3b14a");
        public const string PlaceholderFieldName = "Placeholder";


        public static readonly ID Additional_ParametersFieldId = new ID("5b73a227-c473-41c2-b512-958df50599c1");
        public const string Additional_ParametersFieldName = "Additional Parameters";


        public static readonly ID PersonalizationFieldId = new ID("cf27be1c-313c-4f5c-abc1-07d9bd10b13f");
        public const string PersonalizationFieldName = "Personalization";


        public static readonly ID TestsFieldId = new ID("24ae2e94-98b7-4301-ae0a-7bfa7ac79efc");
        public const string TestsFieldName = "Tests";
    }

    public static partial class Constants
    {
        public const string RENDERING_PARAMETERS___NO_PARAMS_TEMPLATE_ID = "1cda0751-daeb-42c5-b318-2e2f4bb526e8";
    }

    [SitecoreType(TemplateId = "1cda0751-daeb-42c5-b318-2e2f4bb526e8")] //, Cachable = true
    public partial class Rendering_Parameters___No_Params : GlassBase, IRendering_Parameters___No_Params
    {
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
        [SitecoreField("5a0e7c98-d77d-472e-8c2c-1f6ef6878ec7")]
        public virtual object /* UNKNOWN */ Caching { get; set; }

        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
        [SitecoreField("8028dad5-22be-4bc4-96b3-bddfdbf3b14a")]
        public virtual string Placeholder { get; set; }

        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
        [SitecoreField("5b73a227-c473-41c2-b512-958df50599c1")]
        public virtual System.Collections.Specialized.NameValueCollection Additional_Parameters { get; set; }

        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
        [SitecoreField("cf27be1c-313c-4f5c-abc1-07d9bd10b13f")]
        public virtual object /* UNKNOWN */ Personalization { get; set; }

        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Team Development for Sitecore - GlassItem.tt", "1.0")]
        [SitecoreField("24ae2e94-98b7-4301-ae0a-7bfa7ac79efc")]
        public virtual object /* UNKNOWN */ Tests { get; set; }

    }

    
}
