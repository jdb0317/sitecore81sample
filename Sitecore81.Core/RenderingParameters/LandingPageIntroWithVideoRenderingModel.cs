﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore81.Core.Glass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sitecore81.Core.RenderingParameters
{
    [SitecoreType(TemplateId = ILanding_Page_Intro_With_VideoConstants.TemplateIdString, AutoMap = true)]
    public class LandingPageIntroWithVideoRenderingModel
    {
        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Headline_Line_1FieldName)]
        public string Headline_Line_1 { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Headline_Line_2FieldName)]
        public string Headline_Line_2 { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Video_UrlFieldName)]
        public string Video_Url { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Video_CTA_TextFieldName)]
        public string Video_CTA_Text { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Description_HeaderFieldName)]
        public string Description_Header { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Description_TextFieldName)]
        public string Description_Text { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Description_Lower_TextFieldName)]
        public string Description_Lower_Text { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Bottom_CTA_TextFieldName)]
        public string Bottom_CTA_Text { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Bottom_CTA_DestinationFieldName)]
        public Link Bottom_CTA_Destination { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Background_ImageFieldName)]
        public Image Background_Image { get; set; }

        [SitecoreField(ILanding_Page_Intro_With_VideoConstants.Additional_Css_ClassFieldName)]
        public string Additional_Css_Class { get; set; }
    }
}
