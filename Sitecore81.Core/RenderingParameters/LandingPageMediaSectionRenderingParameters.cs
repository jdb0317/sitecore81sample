﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore81.Core.Glass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sitecore81.Core.RenderingParameters
{
    [SitecoreType(TemplateId = ILanding_Page_Media_Section_Rendering_ParametersConstants.TemplateIdString, AutoMap = true)]
    public class LandingPageMediaSectionRenderingParameters
    {
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.UpperHeadlineFieldName)]
        public string UpperHeadline { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.LowerHeadlineFieldName)]
        public string LowerHeadline { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.CopyFieldName)]
        public string Copy { get; set; }

        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video1BackgroundImageFieldName)]
        public Image Video1BackgroundImage { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video1ForegroundTextFieldName)]
        public string Video1ForegroundTextFieldName { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video1SubTextFieldName)]
        public string Video1SubTextFieldName { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video1EmbedCodeFieldName)]
        public string Video1EmbedCodeFieldName { get; set; }

        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video2BackgroundImageFieldName)]
        public Image Video2BackgroundImage { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video2ForegroundTextFieldName)]
        public string Video2ForegroundTextFieldName { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video2SubTextFieldName)]
        public string Video2SubTextFieldName { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video2EmbedCodeFieldName)]
        public string Video2EmbedCodeFieldName { get; set; }

        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video3BackgroundImageFieldName)]
        public Image Video3BackgroundImage { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video3ForegroundTextFieldName)]
        public string Video3ForegroundTextFieldName { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video3SubTextFieldName)]
        public string Video3SubTextFieldName { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video3EmbedCodeFieldName)]
        public string Video3EmbedCodeFieldName { get; set; }

        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video4BackgroundImageFieldName)]
        public Image Video4BackgroundImage { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video4ForegroundTextFieldName)]
        public string Video4ForegroundTextFieldName { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video4SubTextFieldName)]
        public string Video4SubTextFieldName { get; set; }
        [SitecoreField(ILanding_Page_Media_Section_Rendering_ParametersConstants.Video4EmbedCodeFieldName)]
        public string Video4EmbedCodeFieldName { get; set; }
    }
}
