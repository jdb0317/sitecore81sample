﻿using Glass.Mapper.Sc.Fields;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore81.Core.RenderingParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sitecore81.Core.Model
{
    public class LandingPageMediaSectionModel
    {
        public string UpperHeadline { get; set; }
        public string LowerHeadline { get; set; }
        public string Copy { get; set; }
        public IEnumerable<LandingPageMediaSectionVideo> Videos { get; set; }
        public string colClass = "col-md-12 col-xs-12";
        public MediaItem PlayButtonMediaItem { get; set; }
        public string PlayButtonMediaUrl { get; set; }

        private const string playBtnMediaId = "90113B36-86B5-4552-970C-7BC669771F36";

        public LandingPageMediaSectionModel(LandingPageMediaSectionRenderingParameters parameters)
        {
            UpperHeadline = parameters.UpperHeadline;
            LowerHeadline = parameters.LowerHeadline;
            Copy = parameters.Copy;

            Videos = new List<LandingPageMediaSectionVideo> {
                new LandingPageMediaSectionVideo {
                    BackgroundImage = parameters.Video1BackgroundImage,
                    ForegroundText = parameters.Video1ForegroundTextFieldName,
                    SubText = parameters.Video1SubTextFieldName,
                    VideoEmbedCode = parameters.Video1EmbedCodeFieldName,
                },
                new LandingPageMediaSectionVideo {
                    BackgroundImage = parameters.Video2BackgroundImage,
                    ForegroundText = parameters.Video2ForegroundTextFieldName,
                    SubText = parameters.Video2SubTextFieldName,
                    VideoEmbedCode = parameters.Video2EmbedCodeFieldName,
                },
                new LandingPageMediaSectionVideo {
                    BackgroundImage = parameters.Video3BackgroundImage,
                    ForegroundText = parameters.Video3ForegroundTextFieldName,
                    SubText = parameters.Video3SubTextFieldName,
                    VideoEmbedCode = parameters.Video3EmbedCodeFieldName,
                },
                new LandingPageMediaSectionVideo {
                    BackgroundImage = parameters.Video4BackgroundImage,
                    ForegroundText = parameters.Video4ForegroundTextFieldName,
                    SubText = parameters.Video4SubTextFieldName,
                    VideoEmbedCode = parameters.Video4EmbedCodeFieldName,
                },
            }.Where(v => !string.IsNullOrEmpty(v.VideoEmbedCode) && v.BackgroundImage != null);

            var vidCount = Videos.Count();
            colClass = 
                vidCount == 4 ? "col-md-3 col-sm-12"
                : vidCount == 3 ? "col-md-4 col-sm-12"
                : vidCount == 2  ? "col-md-6 col-sm-12"
                : "col-md-12 col-sm-12";

            try
            {
                var item = Sitecore.Context.Database.GetItem(new ID(playBtnMediaId));
                if (item.Paths.IsMediaItem)
                {
                    PlayButtonMediaItem = new MediaItem(item);
                    PlayButtonMediaUrl = Sitecore.Resources.Media.HashingUtils.ProtectAssetUrl(
                        Sitecore.Resources.Media.MediaManager.GetMediaUrl(PlayButtonMediaItem));
                }
            }
            catch
            {
                Sitecore.Diagnostics.Log.Error($"Could not retrieve play_button media img ({playBtnMediaId})", this);
            }

            var x = "fsafds".Count(c => c == 'a');
        }
    }

    public class LandingPageMediaSectionVideo
    {
        public Image BackgroundImage { get; set; }
        public string ForegroundText { get; set; }
        public string SubText { get; set; }
        public string VideoEmbedCode { get; set; }
    }
}
